import React, { Component } from 'react'
import { HashRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Header from './components/header/header.jsx'
import Tab from './components/tab/tab.jsx'
import RouterMap  from './router/index.jsx'

import {getSeller} from './components/api/index'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      seller: {}
    }
  }
  // 定义Context给孙子组件传值
  getChildContext() {
    return {seller: this.state.seller};
  }
  componentWillMount(){
    this.getData()
  }

  getData() {
    getSeller().then(res =>{
      if(res.data.errno === 0 ){
        this.setState({
          seller: Object.assign({},this.state.seller,res.data.data)
        })
      }
    }).catch(err =>{
      console.log(err)
    })
  }
  // componentWillUnMount() {
  //   this.setState = (state,callback)=>{
  //     return;
  //   };
  // }

  render() {
    return (
     
        <HashRouter>
           <React.Fragment>
              <Header key='header' seller={this.state.seller}></Header>
              <Tab key="tab"></Tab>
              <RouterMap key='routerMap' seller={this.state.seller}></RouterMap>
            </React.Fragment>
        </HashRouter>
     
    );
  }

}
// 定义给孙子组件的类型
App.childContextTypes = {
  seller:PropTypes.object
}

export default App;
