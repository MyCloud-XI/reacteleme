import React, { Component } from 'react'
import './cartcontrol.scss'


export default class Cartcontrol  extends Component {
  render() {
    return (
      <div className="cartcontrol">
            <div className={"cart-decrease " + (this.props.food && this.props.food.count ? "move" : "")} onClick={this.decreaseCart.bind(this)}>
                <span className="inner icon-remove_circle_outline"></span>
            </div>
            <div className="cart-count" style={this.props.food && this.props.food.count ? {} : {display : 'none'}}>{this.props.food && this.props.food.count}</div>
            <div className="cart-add" onClick={this.addCir.bind(this)}>
                <span className="inner icon-add_circle"></span>
            </div>
      </div>
    );
  }
  decreaseCart(e) {
      e.stopPropagation();
      if(this.props.food.count){
        this.props.food.count--;
      }

    this.props.cut(e)
  }
  addCir(e) {
    e.stopPropagation();
      if(!this.props.food.count) {
        this.props.food.count = 1;
      }else {
        this.props.food.count++;
      }
      //子组建给父组建传值
      this.props.add(e);
  }
}
