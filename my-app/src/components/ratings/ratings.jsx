import React, { Component } from 'react'
import PropTypes from 'prop-types';
import BScroll from 'better-scroll';

import './ratings.scss';

import Star from '../star/star.jsx';
import Split from '../split/split.jsx';
import Ratingselect from '../ratingselect/ratingselect.jsx';
import {getRatings} from '../api/index.js'
import {formatDate} from '../../assets/js/date.js'

const ERR_OK = 0
const ALL = 2

export default class Ratings  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratings: [],
      selectType: ALL,
      onlyContent: true,
      desc: {
        all: '全部',
        positive: '满意',
        negative: '不满意'
      }
    }
  }

  componentWillMount() {
    getRatings().then((res) => {
      if(res.data.errno === ERR_OK) {
        this.setState({
          ratings: res.data.data
        })
      }
    })
  }
  componentDidMount() {
    this.scroll = new BScroll(this.refs.ratings, { click: true });
  }

  componentDidUpdate() {
      this.scroll.refresh();
  }

  render() {
    return (
        <div className="ratings" ref="ratings">
            <div className="ratings-content">
                <div className="overview">
                    <div className="overview-left">
                        <div>不是</div>
                        <h1 className="score">{this.context.seller.score}</h1>
                        <div className="title">综合评分</div>
                        <div className="renk">高于周边商家{this.context.seller.rankRate}%</div>
                    </div>
                    <div className="overview-right">
                        <div className="score-wrapper">
                            <span className="title">服务态度</span>
                            <Star size="36" score={this.context.seller.serviceScore}></Star>
                            <span className="score">{this.context.seller.serviceScore}</span>
                        </div>
                        <div className="score-wrapper">
                            <span className="title">商品评论</span>
                            <Star size="36" score={this.context.seller.foodScore}></Star>
                            <span className="score">{this.context.seller.foodScore}</span>
                        </div>
                        <div className="delivery-wrapper">
                            <span className="title">送达时间</span>
                            <span className="delivery">{this.context.seller.deliveryTime}分钟</span>
                        </div>
                    </div>
                </div>
                <Split></Split>
                <Ratingselect 
                selectType={this.state.selectType} 
                onlyContent={this.state.onlyContent} 
                ratings={this.state.ratings} 
                desc={this.state.desc}
                select={(type)=>{this.selectRating(type)}}
                toggle={()=>{this.toggleContent()}}
                ></Ratingselect>
                {/* 用户评论详情 */}
                <div className="rating-wrapper">
                  <ul>
                    {
                        this.state.ratings && this.state.ratings.map((rating,index) => (
                          <li className="rating-item" style={this.needShow(rating.rateType, rating.text) ? {} : {display: 'none'}} key={'rating' + index}>
                              <div className="avatar">
                                  <img src={rating.avatar} alt="" width="28" height="28" />
                              </div>
                              <div className="content">
                                  <div className="name">{rating.username}</div>
                                  <div className="star-wrapper">
                                      <Star size="24" score={rating.score}></Star>
                                      <span className="delivery">{rating.deliveryTime}分钟送达</span>
                                  </div>
                                  <p className="text">{rating.text}</p>
                                  <div className="recommend" stype={(rating.recommend && rating.recommend.length) ? {} : {display: 'none'}}>
                                      <span className="icon-thumb_up"></span>
                                      {
                                        rating.recommend && rating.recommend.map((item, index) => (
                                          <span className="item" key={'rating-recommend' + index}>item</span>
                                        ))
                                      }
                                  </div>
                                  <div className="time">
                                      {this.formatDate(rating.rateTime)}
                                  </div>
                              </div>
                          </li>
                        ))
                    }
                  
                  </ul>
                </div>
            </div>
        </div>
    );
  }
  formatDate(time) {
    let date = new Date(time);
    return formatDate(date, 'yyyy-MM-dd hh:mm');
  }
  selectRating(type) {
      this.setState({
        selectType: type
      })
  }
  toggleContent() {
    this.setState({
      onlyContent: !this.state.onlyContent
    })
  }
  needShow(type,text) {
      if(this.state.onlyContent && !text) {
        return false;
      }
      if(this.state.selectType === ALL) {
        return true;
      } else {
        return this.state.selectType === type;
      }
  }
}

Ratings.contextTypes = {
  seller: PropTypes.object
};
