import React, { Component } from 'react'
import BScroll from 'better-scroll'

import Cartcontrol from '../cartcontrol/cartcontrol.jsx'
import Split from '../split/split.jsx'
import Ratingselect from '../ratingselect/ratingselect.jsx'
import {formatDate} from '../../assets/js/date.js'

import './food.scss'

const ALL = 2;

export default class Food  extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showFlag: false,
      selectType: ALL,
      onlyContent: true,
      desc: {
        all: '全部',
        positive: '推荐',
        negative: '吐槽'
      }
    }
  }

  componentDidMount() {
    this.scroll = new BScroll(this.refs.food,{click: true});
  }
  componentDidUpdate() {
    this.scroll.refresh();
  }
  render() {
    return (
      <div className={"food " + (this.state.showFlag ? '' : ' hide')} ref="food">
          <div className="food-content">
              {/* 商品图片 */}
              <div className="image-header">
                  <img src={this.props.food.image} alt=""/>
                  <div className="back" onClick={() => {this.hide()}}>
                    <span className="icon-arrow_lift"></span>
                  </div>
              </div>
              <div className="content">
                  <h1 className="title">{this.props.food.name}</h1>
                  <div className="detail">
                      <span className="sell-count">月销{this.props.food.sellCount}</span>
                      <span className="rating">好评{this.props.food.rating}</span>
                  </div>
                  <div className="price">
                      <span className="now">${this.props.food.price}</span><span className="old">{this.props.food.oldprice}</span>
                  </div>
                  <div className="cartcontrol-wrapper">
                      <Cartcontrol food={this.props.food} add={(e)=>{this.addFood(e)}} cut={(e)=>{this.cutFood(e)}}></Cartcontrol>
                  </div>
                  <div className="buy" onClick={(e) => {this.addFirst(e);}} style={(!this.props.food.count || this.props.food.count===0) ? {} : {display: 'none'}}>加入购物车</div>
              </div>
              <Split></Split>
              <div className="info">
                <h1 className="title">商品信息</h1>
                <p className="text">{this.props.food.info}</p>
              </div>
              <Split></Split>
              <div className="rating">
                  <h1 className="title">商品评价</h1>
                  <Ratingselect 
                    selectType={this.state.selectType}
                    onlyContent={this.state.onlyContent}
                    ratings={this.props.food.ratings}
                    desc={this.state.desc}
                    select={(type) => {this.selectRating(type);}}
                    toggle={this.toggleContent.bind(this)}
                  ></Ratingselect>
                  {/* 评论信息 */}
                  <div className="reting-wrapper">
                      <ul>
                        {
                          this.props.food.ratings && this.props.food.ratings.map((rating,index) => (
                                <li className="rating-item" key={'rating' + index} style={this.needShow(rating.rateType, rating.text) ? {} : {display: 'none'}}>
                                  <div className="user">
                                      <span className="name">{rating.username}</span>
                                      <img src={rating.avatar} alt=""/>
                                  </div>
                                  <div className="tiem">{this.formatDate(rating.rateTime)}</div>
                                  <p className="text">
                                      <span className={(rating.rateType===0) ? 'icon-thumb_up' : (rating.rateType===1 ? 'icon-thumb_down' : '')} ></span>{rating.text}
                                  </p>
                              </li>
                          ))
                        } 
                      </ul>
                      <div className="no-rating" style={(!this.props.food.ratings || !this.props.food.ratings.length) ? {} : {display: 'none'}}>暂无评价</div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
  formatDate(time) {
    let date = new Date(time);
    return formatDate(date, 'yyyy-MM-dd hh:mm');
  }
  show() {
      this.setState({
        showFlag:true,
        selectType: ALL,
        onlyContent: true,
      })
  }
  hide() {
    this.setState({
      showFlag:false
    })
  }
  selectRating(type) {
    this.setState({
      selectType: type
    })
  }
  toggleContent() {
    this.setState({
      onlyContent: !this.state.onlyContent
    })
  }
  needShow(type, text) {
      if(this.state.showFlag === false){
        return;
      }

      if(this.state.onlyContent && !text){
        return false;
      }

      if(this.state.selectType === ALL){
        return true;
      }else{
        return this.state.selectType === type
      }
  }
  addFirst(e) {
      this.props.food.count = 1;
      this.props.add(e)
  }
  addFood(e){
    this.props.add(e)
  }
  cutFood(e){
    this.props.cut(e)
  }
}
