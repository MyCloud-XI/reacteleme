import React, { Component } from 'react'
import BScroll from 'better-scroll';
import PropTypes from 'prop-types';
import './goods.scss'

import Cartcontrol from '../cartcontrol/cartcontrol.jsx'
import Shopcart from '../shopcart/shopcart.jsx'
import Food from '../food/food.jsx'
import {getGoods} from '../api/index'


export default class Goods  extends Component {
    constructor(props) {
      super(props);
      this.state = {
        goods: [],
        classMap: ["decrease", "discount", "special", "invoice", "guarantee" ],
        listHeight: [],
        foodList: [],
        menuList: [],
        scrollY: 0,
        selectFoods: [],
        selectFood: {},
        Index: 0
      }
    }
    componentWillMount() {
      this.getData()
    }
    componentDidMount() {
      this._initCsroll()
      setTimeout(()=> {
        this._calculateHeight();
      },50)
    }
    // componentWillUnMount() {
    //   this.setState = (state,callback)=>{
    //     return;
    //   };
    // }
    //获取数据goods
    getData() {
      getGoods().then((res) => {
        if(res.data.errno === 0) {
          this.setState({
            goods: res.data.data
          })
        }
      }).catch((err) => {
        console.log(err)
      })
    }
    _initCsroll() {
      this.meunScroll = new BScroll(this.refs.menuwrapper,{click: true});

      this.foodsScroll = new BScroll(this.refs.foodswrapper,{
        click: true,
        probeType: 3
      });

      this.foodsScroll.on('scroll',(pos) => {
          // 判断滑动方向，避免下拉时分类高亮错误（如第一分类商品数量为1时，下拉使得第二分类高亮）
          if(pos.y <= 0) {
              this.setState({
                scrollY: Math.abs( Math.round(pos.y))
              });
          }
      });
    }
    render() {
      return (
        <div className="goods">
            {/* 商品页左侧栏 */}
            <div className="menu-wrapper" ref="menuwrapper">
                <ul>
                  {
                    this.state.goods.length > 0 && this.state.goods.map((item, index) => (
                      <li className={"men-item " + (this.currentIndex() === index ? 'current' : '')} key={'menuList' + index} ref={(node) => {this.state.menuList[index] = node;}} onClick={(ev)=> {this.selectMenu(index,ev);}}>
                        <span className="text">
                          <span className={"icon " + this.state.classMap[item.type]}  style={item.type > 0 ? {}: {display: 'none'}}></span>{item.name}
                        </span>
                        {/* {this.state.Index} */}
                      </li>
                    ))
                  }
                </ul>
            </div>
            {/* 商品页右侧 */}
            <div className="foods-wrapper" ref="foodswrapper">
                  <ul>
                      {
                        this.state.goods.length > 0 && this.state.goods.map((item,index) => (
                          <li className="food-list" key={'foodList' + index} ref={node=>{this.state.foodList[index]=node;}}>
                              <h1 className="title">{item.name}</h1>
                              <ul>
                                {
                                  item.foods.map((food,index) => (
                                    <li onClick={(e) => {this.selectFood(food, e);}} className="food-item" key={'food' + index}>
                                      <div className="icon">
                                        <img src={food.icon} alt="" width="57" height="57"/>
                                      </div>
                                      <div className="content">
                                          <h2 className="name">{food.name}</h2>
                                          <p className="desc">{food.description}</p>
                                          <div className="extra">
                                              <span className="count">月销{food.sellCount}</span><span>好评率{food.rating}</span>
                                              <div>sssssssssss</div>
                                          </div>
                                          <div className="price">
                                            <span className="now">¥{food.price}</span><span className="old">{food.oldprice}</span>
                                          </div>
                                          <div className="cartcontrol-wrapper">
                                              <Cartcontrol food={food}  add={this.addFood.bind(this)}  cut={this.cutFood.bind(this)}></Cartcontrol>
                                          </div>
                                      </div>
                                    </li>
                                  ))
                                }
                              </ul>
                          </li>
                        ))
                      }
                  </ul>
            </div>
            {/* 购物车组建 */}
            <Shopcart ref="shopcart"
            add={this.addFood.bind(this)}
            cut={this.cutFood.bind(this)}
            selectFoods={this.state.selectFoods}
            deliveryPrice={this.context.seller.deliveryPrice}
            minPrice={this.context.seller.minPrice}
            empty={this.fempty.bind(this)}
            ></Shopcart>

            <Food food={this.state.selectFood} ref="food" add={(e) => {this.addFood(e);}} cut={(e)=>{this.cutFood(e)}}></Food>
        </div>
      );
    }
    currentIndex() {
      let listHeight = this.state.listHeight
      for(let i = 0; i < listHeight.length; i++){
        let height1 = listHeight[i];
        let height2 = listHeight[i+1];
        if(!height2 || (this.state.scrollY >= height1 && this.state.scrollY < height2)) {
          this._followScroll(i);
          return i ;
        }
      }
      return 0;
    }
    _calculateHeight() {
      let foolist = this.state.foodList;
      let height = 0;
      this.state.listHeight.push(height);
      foolist.forEach((li,index) => {
        height += li.clientHeight;
        this.state.listHeight.push(height);
      })
    }
    selectMenu(index) {
      let foodList =this.state.foodList;

      this.foodsScroll.scrollToElement(foodList[index],300)
    }
    _followScroll(index) {
        let menuList = this.state.menuList;

        this.meunScroll.scrollToElement(menuList[index],300)
    }
    addFood(e) {
      // react重现渲染的方法
      this.selectFoods();
      this.forceUpdate();
      this.refs.shopcart.drop(e.target)
    }
    cutFood() {
      this.selectFoods();
      this.forceUpdate();
    }
    fempty() {
      this.selectFoods();
      this.forceUpdate();
    }
    selectFoods() {
      const foods = [];
      this.state.goods.forEach(good => {
        good.foods.forEach(food => {
          if(food.count) {
            foods.push(food)
          }
        })
      })
      this.setState({
        selectFoods: foods
      })
    }
    selectFood(food,e) {
        e.stopPropagation();
        this.setState({selectFood: food});
        this.refs.food.show();
    }
}

// 上下文类型
Goods.contextTypes = {
  seller: PropTypes.object
};
