import React from 'react'
import BScroll from 'better-scroll';

import './shopcart.scss';
import Cartcontrol from '../cartcontrol/cartcontrol.jsx';


export default class Shopcart  extends React.Component {
    constructor(props) {
        super(props);
        this.state = {fold: true};
        // this.ball = React.createRef();
        // this.innerBall = React.createRef();
    }
    // 在第一次渲染后调用，只在客户端。之后组件已经生成了对应的DOM结构
    componentDidMount() {
        this.scroll = new BScroll(this.refs.listContent,{click: true});
    }
    // 在组件接收到一个新的 prop (更新后)时被调用
    componentWillReceiveProps(nextProps) {
        if (nextProps.selectFoods) {
            let count = 0;
            nextProps.selectFoods.forEach((food) => {
                count += food.count;
            });
            if (count === 0) this.setState({fold: true});
        }
    }
    render() {
        return (
        <div>
            <div className="shopcart" onClick={this.toggleList.bind(this)}>
                {/* 购物车内容部分 */}
                <div className="cotent">
                        <div className="cotent-letf">
                            <div className="logo-wrapper">
                                <div className={"logo " + (this.totaCount() > 0 ? "highlight" : "")}>
                                    <i className={"icon-shopping_cart " + (this.totaCount() > 0 ? "highlight" : "")}></i>
                                </div>
                                <div className="nums" style={this.totaCount() >0 ? {}: {display: 'none'}}>{this.totaCount()}</div>
                            </div>
                            <div className={"price " + (this.totaCount() > 0 ? "highlight" : "")}>¥{this.totaPrice()}</div>
                            <div className="decs">另配送费{this.props.deliveryPrice}元</div>
                        </div>
                        <div className="comtent-right">
                            <div className={"pay " + (this.payClass())}>{this.payDesc()}</div>
                        </div>
                </div>
                {/* 小球部分 */}
                <div className="ball" ref="ball" onTransitionEnd={this.resetTrans.bind(this)}>
                    <div className="inner" ref="inner"></div>
                </div>
                 {/* 向上升起的购物列表 */}
                <div className={"shopcart-list " + (this.listShow() ? "fade" : "")}>
                    <div className="list-header">
                        <h1 className="title">购物车</h1>
                        <span className="empty" onClick={this.empty.bind(this)}>清空</span>
                    </div>
                    <div className="list-content" ref="listContent">
                        <ul>
                            {
                                this.props.selectFoods && this.props.selectFoods.map((food,index) => (
                                    <li className="food" key={'food'+ index}>
                                        <span className="name">{food.name}</span>
                                        <div className="price">
                                            <span>¥{food.price*food.count}</span>
                                        </div>
                                        <div className="certontrol-wrapper">
                                            <Cartcontrol food={food} add={this.addFood.bind(this)}  cut={this.cutFood.bind(this)}></Cartcontrol>
                                        </div>
                                    </li>
                                ))
                            }
                            
                        </ul>
                    </div>
                </div>
            </div>
            {/* 遮罩层 */}
            <div className={"list-mask " + (this.listShow() ? "enter" : "")} onClick={this.hideList.bind(this)}></div>
        </div>
        );
    }
    drop(el) {
        this.resetTrans();
        const rect = el.getBoundingClientRect();
        const rx = rect.left - 28;
        const ry = window.innerHeight - rect.top - 20;

        this.refs.ball.style.visibility = 'visible';
        this.refs.ball.style.left = `${rect.left}px`;
        this.refs.ball.style.top = `${rect.top}px`;
        this.refs.ball.style.transform = `translate(-${rx}px, 0)`;
        this.refs.ball.style.transition = 'transform .5s linear';

        this.refs.inner.style.transform = `translate(0, ${ry}px)`;
        this.refs.inner.style.transition = 'transform 0.5s cubic-bezier(0.5, -0.5, 1, 1)';
        
    }
    resetTrans() {
        this.refs.ball.style.visibility = 'hidden';
        this.refs.ball.style.transform = 'none';
        this.refs.ball.style.transition = 'none';

        this.refs.inner.style.transform = 'none';
        this.refs.inner.style.transition = 'none';
    }
    totaCount() {
        let count = 0;
        this.props.selectFoods.forEach(food => {
            count+= food.count;
        });
        return count;
    }
    totaPrice() {
        let total = 0;
        this.props.selectFoods.forEach(food => {
            total+= food.count * food.price;
        });
        return total;
    }
    payDesc() {
        if(this.totaCount === 0) {
            return `${this.props.minPrice}起送`;
        }else if(this.totaPrice() < this.props.minPrice) {
            let diff = this.props.minPrice - this.totaPrice()
            return `还差${diff}元起送`
        }else {
            return "结算"
        }
    }
    payClass() {
        if(this.totaPrice() >= this.props.minPrice) {
            return "enough"
        }else {
            return "not-enough "
        }
    }
    empty(e) {
        e.stopPropagation();
        this.props.selectFoods.forEach(food => {
            food.count = 0;
        });
        this.props.empty()
    }
    listShow() {
        if(!this.totaCount()) {
            return false;
        }

        let show = !this.state.fold;

        return show;
    }
    toggleList() {
        if(!this.totaCount()) return;

        this.setState({
            fold: !this.state.fold
        })
    }
    addFood(e) {
        this.props.add(e)
    }
    cutFood(e) {
        this.props.cut(e)
    }
    hideList() {
        this.setState({
            fold: !this.state.fold
        })
    }
}

Shopcart.defaultProps = {
    selectFoods: [],
    deliveryPrice: 0,
    minPrice: 0
};
