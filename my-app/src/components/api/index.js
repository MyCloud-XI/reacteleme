import axios from 'axios';

export function getSeller(callback) {
    return new Promise(function(resolve, reject) {
        axios.get('api/seller')
            .then(function(res){
                resolve(res)
            })
            .catch(function(error){
                reject(error)
            })
    })  
}

export function getGoods(callback) {
    return new Promise(function(resolve, reject) {
        axios.get('api/goods')
            .then(function(res) {
                resolve(res)
            })
            .catch(function(error) {
                reject(error)
            })
    })
}

export function getRatings(callback) {
    return new Promise(function(resolve, reject) {
        axios.get('api/ratings')
            .then(function(res) {
                resolve(res)
            })
            .catch(function(error) {
                reject(error)
            })
    })
}  
