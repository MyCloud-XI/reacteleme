import React, { Component } from 'react'
import {Route, Switch, Redirect } from 'react-router-dom'

import Goods from '../components/goods/goods.jsx'
import Ratings from '../components/ratings/ratings.jsx'
import Seller from '../components/seller/seller.jsx'


export default class Header  extends Component {
  render() {
    return (
        <Switch>
            <Route exact path="/goods" component={Goods}></Route>
            <Route exact path="/ratings" component={Ratings}></Route>
            <Route exact path="/seller" component={Seller}></Route>
            <Redirect to='/goods' />
        </Switch>
    );
  }
}
