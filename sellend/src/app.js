const express = require("express")
const axios = require('axios')

const appData = require('../data.json')

const seller = appData.seller
const goods = appData.goods
const ratings = appData.ratings

// 创建服务
const app = express()

//创建路由
const Routers = express.Router()

Routers.get("/seller",(req,res)=>{
    return res.json({
        errno:0,
        data:seller
    })
})

Routers.get("/goods",(req,res)=>{
    return res.json({
        errno:0,
        data:goods
    })
})

Routers.get("/ratings",(req,res)=>{
    return res.json({
        errno:0,
        data:ratings
    })
})

Routers.get("/stock",(req,res)=>{
    return res.json({
        errno:0,
        data:ratings
    })
})





// let appid = 'wxcdeb693f422fd196'
// let appsecret = '364676ba84bee96f3e3368fa049676e9'

// const access_token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='+appid+'&secret='+appsecret+''
// axios.get(access_token_url).then((res) => {
//     getmenu(res.data.access_token)
// })

// function getmenu(token) {
//     const menu_url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token='+token+''
//     const menu = {
//         button:[
//         {    
//             type:"miniprogram",
//             name:"wxa",
//             url:"http://mp.weixin.qq.com",
//             appid:"wx286b93c14bbf93aa",
//             pagepath:"pages/lunar/index"
//         },
//         {
//             name:"菜单",
//             sub_button:[
//             {    
//                 type:"view",
//                 name:"首页",
//                 url:"http://dev-eleme.xiaoying.love/api/scan?url=goods"
//             },
//             {
//                 type:"view",
//                 name:"商家",
//                 url:"http://dev-eleme.xiaoying.love/api/scan?url=seller"
//             },
//             {
//                 type:"view",
//                 name:"评论",
//                 url:"http://dev-eleme.xiaoying.love/api/scan?url=ratings"
//             }]
//         }]
//     }
//     axios.post(menu_url, menu).then((res) => {
//         console.log(res.data)
//     })
// }


// Routers.get("/scan",(req,res, next) => {
//     var router = '/api/get_wx_access_token'
//     var return_uri = 'http://dev-eleme.xiaoying.love' + router + '?url='+req.query.url+''
//     console.log(return_uri)
//     var scope = 'snsapi_base';
//     var code_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='+appid+'&redirect_uri='+encodeURIComponent(return_uri)+'&response_type=code&scope='+scope+'&state=STATE#wechat_redirect'

//     return res.redirect(code_url)
// })

// Routers.get("/get_wx_access_token",(req,res, next) => {
//     var code = req.query.code;
//     var url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='+appid+'&secret='+appsecret+'&code='+code+'&grant_type=authorization_code'
//     axios.get(url).then((response) => {
//         console.log(response.data)
//     })
//     return res.redirect("http://dev-eleme.xiaoying.love/#/" +req.query.url)
// })


app.use('/api',Routers)

// 开启服务
app.listen(30000,"127.0.0.1",(err)=>{
    if(err){
        console.log(err)
    }else{
        console.log("启动成功")
    }
})